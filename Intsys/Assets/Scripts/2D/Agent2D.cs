﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using UnityEngine.Assertions.Must;

public class Agent2D : Agent
{

    Rigidbody2D rBody;
    [SerializeField]
    public GameObject Goal;
    [SerializeField]
    public GameObject Coin;
    [SerializeField]
    public UiController ui;

    // Script responsible for handling movement
    public CharacterController2D playerController;
    public float runSpeed = 40f;

    public float maxReward;

    bool jump = false;

    void Start()
    {
        rBody = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// Actions to take on begin every episode
    /// </summary>
    public override void OnEpisodeBegin()
    {
        ui.setEpisodeText("Episode: " + (this.CompletedEpisodes + 1));
        this.rBody.angularVelocity = 0;
        this.rBody.velocity = Vector2.zero;
        this.transform.localPosition = new Vector3(1f, 1f, 0);

        // Get observation objects
        Goal = GameObject.Find("Goal");
        Coin = GameObject.Find("Coin");

        Coin.GetComponent<Coin>().collected = false;
        Coin.GetComponent<SpriteRenderer>().enabled = true;

        Debug.Log("Completed Episodes:");
        Debug.Log(this.CompletedEpisodes);
    }

    /// <summary>
    /// Collects Oberservations for the Agent
    /// </summary>
    /// <param name="sensor">VectorSensor</param>
    public override void CollectObservations(VectorSensor sensor)
    {
       
        // Goal Coords
        sensor.AddObservation(Goal.transform.localPosition.x);

        // Coin Coords
        sensor.AddObservation(Coin.transform.localPosition.y);
        sensor.AddObservation(Coin.transform.localPosition.x);

        // Agent positions
        sensor.AddObservation(this.transform.localPosition.x);
        sensor.AddObservation(this.transform.localPosition.y);

        // Agent velocity
        sensor.AddObservation(rBody.velocity.x);
    }

    /// <summary>
    /// Recives the Actions from the Brain
    /// </summary>
    /// <param name="vectorAction"></param>
    public override void OnActionReceived(float[] vectorAction)
    {
        // Move Agent
        MoveAgent(vectorAction);

        float distanceToTarget = System.Math.Abs(this.transform.localPosition.x - Goal.transform.localPosition.x);
        float distanceToCoin = Vector3.Distance(this.transform.localPosition, Coin.transform.localPosition);

        // If player collects the coin, add reward
        
        if (distanceToCoin < 5f && !Coin.GetComponent<Coin>().collected)
        {
            AddReward(0.05f);
            Coin.GetComponent<Coin>().collected = true;
            Coin.GetComponent<SpriteRenderer>().enabled = false;
        }

        // If Agent is near the end
        if (distanceToTarget < 12f)
        {
            SetReward(1.0f);
            this.setHighscore();
            ui.setLastEpisodeMaxReward("Highscore last Episode: " + (this.GetCumulativeReward()));
            EndEpisode();
        }

        // Fell off platform
        if (this.transform.localPosition.y < 0)
        {
            SetReward(-1.0f);
            this.setHighscore();
            ui.setLastEpisodeMaxReward("Highscore last Episode: " + (this.GetCumulativeReward()));
            EndEpisode();
        }

        // If player is out of screen on the y-axis end episode
        if (this.transform.localPosition.y > 100.0f)
        { 
            SetReward(-1.0f);
            this.setHighscore();
            ui.setLastEpisodeMaxReward("Highscore last Episode: " + (this.GetCumulativeReward()));
            EndEpisode();
        }

        // If player is out of screen on the x-axis end episode
        if(this.transform.localPosition.x < -5f)
        {
            SetReward(-1.0f);
            this.setHighscore();
            ui.setLastEpisodeMaxReward("Highscore last Episode: " + (this.GetCumulativeReward()));
            EndEpisode();
        }

        this.setHighscore();

        ui.setStepText("Time: " + (this.StepCount) + " / Max: " + (this.MaxStep));
        ui.setRewardText("Reward: " + (this.GetCumulativeReward()));
        

    }

    public void setHighscore()
    {
        if(this.GetCumulativeReward() > this.maxReward)
        {
            this.maxReward = this.GetCumulativeReward();
        }
        
        ui.setMaxRewardText("Highscore: " + (this.maxReward));
    }

    /// <summary>
    /// Method which handles the Action to take for the Agent
    /// </summary>
    /// <param name="act"></param>
    public void MoveAgent(float[] act)
    {
        // Decrease reward for every step taking, to encourage the optimal path
        AddReward(-0.0005f);
       
        var dirToGo = Vector3.zero;
        var dirToGoForwardAction = (int)act[0];
        var jumpAction = (int)act[1];

        // Movement to the left
        if (dirToGoForwardAction == 1)
        {
            AddReward(-0.0015f);
            dirToGo = -1f * transform.right;
            
        }
        // Movement to the right
        else if (dirToGoForwardAction == 2)
        {
            dirToGo = 1f * transform.right;
        }
        // Jump
        if (jumpAction == 1 && !jump)
        {
            AddReward(-0.00025f);
            jump = true;
        }
        // else no action

        float distanceToTarget = System.Math.Abs(this.transform.localPosition.x - Goal.transform.localPosition.x);

        // Increase Rewards towards Goal
        AddReward(0.0025f * (230/distanceToTarget));
        
        // Execute Movement
        playerController.Move(dirToGo.x * Time.fixedDeltaTime * runSpeed, jump);

        jump = false;

        Debug.Log("Steps:");
        Debug.Log(this.StepCount);

        Debug.Log("Rewards:");
        Debug.Log(this.GetCumulativeReward());
    }

    /// <summary>
    /// Heuristic method for self-play
    /// </summary>
    /// <param name="actionsOut"></param>
    public override void Heuristic(float[] actionsOut)
    {
        if (Input.GetKey(KeyCode.A))
        {
            actionsOut[0] = 1f;
        } else if(Input.GetKey(KeyCode.D)) {
            actionsOut[0] = 2f;
        } else
        {
            actionsOut[0] = 0f;
        }
        
        actionsOut[1] = Input.GetKey(KeyCode.Space) ? 1.0f : 0.0f;
        
    }
    
}
