﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// Benutzung!! 
/// - Text Objekt als Kind des Canvas Objektes anlegen
/// - Das Text Objekt dem Controller bekannt machen
/// - Über den Agent an den Passenden Stellen den Text über den Controller updaten
/// - Dazu im Controller passende Methoden bereit stellen
/// - Beispielhaft für Episoden gemacht
/// </summary>
public class UiController : MonoBehaviour
{
    [SerializeField]
    Text episodeText;

    [SerializeField]
    Text stepText;

    [SerializeField]
    Text rewardText;

    [SerializeField]
    Text maxRewardText;

    [SerializeField]
    Text lastEpisodeMaxRewardText;

    public void setEpisodeText(string _val)
    {
        episodeText.text = _val;
    }

    public void setStepText(string _val)
    {
        stepText.text = _val;
    }

    public void setRewardText(string _val)
    {
        rewardText.text = _val;
    }

    public void setMaxRewardText(string _val)
    {
        maxRewardText.text = _val;
    }

    public void setLastEpisodeMaxReward(string _val)
    {
        lastEpisodeMaxRewardText.text = _val;
    }



}
