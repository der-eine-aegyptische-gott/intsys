## <u>Requirements</u>

- Windows 10 
- [Unity 2019.3.14f1 oder höher/später](https://unity3d.com/get-unity/download/archive)
- [ML-Agents Release_2](https://github.com/Unity-Technologies/ml-agents/blob/release_2_docs/docs/Installation.md)

## <u>Setup</u>

- Projekt aus dem Git Klonen / oder Entpacken
- Den Projekt Ordner ``Intsys`` in Unity öffnen
- Die Jumpnrun Scene in Unity öffen
- Das Projekt ausführen

## <u>Train Agents</u>

##### <u>Optimizers</u>:

###### - <u>PPO (Proximal Policy Optimization):</u>

- ``mlagents-learn jump_n_run_agent.yaml --run-id=JumpRunAgent --force`` ausführen um ein neues Training zu starten - alternativ ``intsys/assets/trainer/learn_new_ppo.cmd``
- ``mlagents-learn jump_n_run_agent.yaml --run-id=JumpRunAgent --resume`` ausführen um ein Training wiederaufzunehmen - alternativ ``intsys/assets/trainer/learn_resume_ppo.cmd``

###### - <u>SAC (Soft Actor-Critic):</u>

- ``mlagents-learn jump_n_run_agent.yaml --run-id=JumpRunAgentSAC --force`` ausführen um ein neues Training zu starten - alternativ ``intsys/assets/trainer/learn_new_sac.cmd``
- ``mlagents-learn jump_n_run_agent.yaml --run-id=JumpRunAgentSAC --resume`` ausführen um ein Training wiederaufzunehmen - alternativ ``intsys/assets/trainer/learn_resume_sac.cmd``
