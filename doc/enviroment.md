# Environment

Die umgebung des Agenten setzt sich aus mehreren Elementen zusammen

- Steinsäule ( Hinderniss )

- Münze ( Postive Reward )

- Boden

- Ziel ( Postive Reward beim erreichen )

- UI-Elemente 
  
  - Episoden Anzahl
  
  - Zeit/Steps - current / max
  
  - Current Reward
  
  - Highscore ( über alle Episoden hinweg )
  
  - Highscore last Episode

---

<img src="images\enviroment_01.png" title="" alt="" data-align="center">
