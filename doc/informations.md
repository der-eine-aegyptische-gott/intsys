# <u>Informations</u>

## <u>Rewards</u>

### <u>Positiv</u>

- Jeder Schritt in richtung Ziel (+0.0025f * (% der geschafften strecke))
- Münze (+0.5f)
- Ziel (= +1.0f)

### <u>Negativ</u>

- Jeder Schritt kumulativ (-0.0005f)
- außerhalb des Spielfelds (= -1.0f)

---

## <u>Actions</u>:

- X-Axis [3 Actions]:
  - 0 = NO ACTION
  - 1 = LEFT
  - 2 = RIGHT
- Jump [2 Actions]:
  - 0 = NO ACTION
  - 1 = JUMP

---

## <u>Data access</u>

- Lokale Position des Agents 
- Lokale Position des Zieles
- Lokale Position der Münzen
