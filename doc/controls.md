# <u>Controls</u>

## <u>Control (Self-Play)</u>

- A/D => Laufen
- Space => Springen

## <u>Control (ML-Agents)</u>

- Das Player2D_Agent Objekt im Inspector öffnen
- Behavior Type in den Behavior Parameters von `Heuristic Only` auf `Default` ändern
- Wenn kein Brain vorhanden ist, entweder Trainieren lassen, oder eins der mit den Optimizern Trainierten Brains `JumpRunAgentPPO_FINAL.nn` | `JumpRunAgentSAC_FINAL.nn` auswählen. Diese befinden sich im Ordner `./Assets/Trainer/models`.
