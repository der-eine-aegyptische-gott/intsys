# <u>Jumpin’ Agent</u>

## <u>Documentation</u>

Git Repository für das Projekt für Intelligente Systeme - CodeName: **<u>Jumpin’Agent</u>**

## <u>Zielsetzung</u>

Das ziel dieses Projektes ist es eine Super Mario Bros ähnliche umgebung zu schaffen und in dieser einen Agenten mittels dem Machine Learning Toolkit von Unity (ML-Agents) zu trainieren, so das es diesem möglich ist die umgebung Perfomant zu meistern.

Um festzustellen ob dies auch wirklich der fall ist, gibt es in der Umgebung passende UI-Elemente (HighScore), an desen man die verschiendene Optimizers und das manuelle meistern der Umgebung vergleichen kann. 

---

## [Setup](setup.md)

---

## [Enviroment](enviroment.md)

---

## [Training](training.md)

---

## [Controls](controls.md)

---

## [Informations](informations.md)

---
